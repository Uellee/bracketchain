package main;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;

public class Controller {

    @FXML
    public TextArea expressionTextArea;

    public void onKeyReleased(KeyEvent keyEvent) {
        String s = expressionTextArea.getText();
        calc(s);
    }

    private void calc(String s) {
        if (WordChecker.check(s)) onSucceedCheck();
        else onFailCheck();
    }

    private void onFailCheck() {
        expressionTextArea.setStyle("-fx-background-color: #ffaaaa");
    }

    private void onSucceedCheck() {
        expressionTextArea.setStyle("-fx-background-color: #aaffaa");
    }
}
