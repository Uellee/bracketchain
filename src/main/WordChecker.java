package main;

import java.util.Arrays;
import java.util.Stack;

/**
 * Created by DEA on 02.03.17.
 */
class WordChecker {

    static boolean check(String s) {

        Stack<Bracket> stack = new Stack<>();

        for (int i = 0; i < s.length(); i++) {

            Bracket bracket;
            try {
                bracket = Bracket.builder(s.charAt(i));
            } catch (Exception e) {
                return false;
            }

            int condition = 0;
            if (null == bracket) condition += 1;
            if (!stack.empty()) condition += 2;

            switch (condition) {
                case 0:
                    stack.push(bracket);
                    break;
                case 1:
                    return false;

                case 2:
                    stack.push(bracket);
                    break;
                case 3: {
                    if (stack.peek().isRev(s.charAt(i))) stack.pop();
                    else return false;
                }
            }
        }

        return stack.isEmpty();
    }
}

class Bracket {

    private final char _c;

    private Bracket(char c) {
        this._c = c;
    }

    boolean isRev(char c) {

        switch (_c) {
            case '(':
                return c == ')';
            case '[':
                return c == ']';
            case '{':
                return c == '}';
        }

        return false;
    }

    static Bracket builder(char c) throws Exception {

        if (Arrays.asList('(', '[', '{').contains(c)) {
            return new Bracket(c);
        }

        if (Arrays.asList(')', ']', '}').contains(c)) {
            return null;
        }

        throw new Exception();
    }

}